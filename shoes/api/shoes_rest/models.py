from django.db import models


class binVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)


class Shoes(models.Model):
    maunfacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField(null=True)
    bin = models.ForeignKey(
        binVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name
