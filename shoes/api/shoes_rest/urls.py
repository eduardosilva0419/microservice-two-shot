from django.urls import path, include
from .views import list_shoes, delete_shoes, detail_shoes

urlpatterns = [
    path("shoes/", list_shoes, name="list_shoes"),
    path("shoes/<int:id>", delete_shoes, name="delete_shoes"),
    path("shoes/<int:id>", detail_shoes, name="detail_shoes"),


]
