from django.shortcuts import render
from .models import Shoes, binVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.shortcuts import get_object_or_404

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["name", "picture", "id", "color", "maunfacturer"]




class ShoesDetailsEncoder(ModelEncoder):
    model = Shoes
    properties = ["maunfacturer", "name", "color", "picture", "id"]

    def get_extra_data(self, o):
        return {"bin": o.bin.import_href}

@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"Shoes": shoes},
            encoder= ShoesListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            new_bin = binVO.objects.get(import_href=content['bin'])
            content['bin'] = new_bin
        except binVO.DoesNotExist:
            return JsonResponse(
                {"message": "bad bin"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailsEncoder,
            safe=False,
        )


def delete_shoes(request, id):
    if request.method == "DELETE":
        shoes = get_object_or_404(Shoes, id=id)
        shoes.delete()
        return JsonResponse(
            {"deletion": f"you deleted it.."},
        )

@require_http_methods(["GET"])
def detail_shoes(request, id):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailsEncoder,
            safe=False
        )
