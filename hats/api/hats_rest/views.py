from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from hats_rest.models import Hat, LocationVO
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "id",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "material",
        "color",
        "size",
        "picture",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(HatListEncoder):
    properties = [
        "style",
        "material",
        "color",
        "size",
        "picture",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_hats_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)

    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location_href"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hats_detail(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(pk=pk)
        return JsonResponse({"hat": hat}, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse({"hat": hat}, encoder=HatDetailEncoder, safe=False)
