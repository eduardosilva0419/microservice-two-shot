import React, { useEffect, useState } from "react";

function HatForm() {
	const [style, setStyle] = useState("");
	const [material, setMaterial] = useState("");
	const [color, setColor] = useState("");
	const [size, setSize] = useState("");
	const [location, setLocation] = useState("");
	const [locations, setLocations] = useState([]);
	const [picture, setPicture] = useState([]);

	const handleStyleChange = (event) => {
		const value = event.target.value;
		setStyle(value);
	};

	const handleMaterialChange = (event) => {
		const value = event.target.value;
		setMaterial(value);
	};

	const handleColorChange = (event) => {
		const value = event.target.value;
		setColor(value);
	};

	const handleSizeChange = (event) => {
		const value = event.target.value;
		setSize(value);
	};

	const handleLocationChange = (event) => {
		const value = event.target.value;
		setLocation(value);
	};

	const handlePictureChange = (event) => {
		const value = event.target.value;
		setPicture(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.style = style;
		data.material = material;
		data.color = color;
		data.size = size;
		data.location = location;
		data.picture = picture;
		console.log(data);

		const url = "http://localhost:8090/api/hats/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			const newHat = await response.json();
			event.target.reset(newHat);

			setStyle("");
			setMaterial("");
			setColor("");
			setSize("");
			setLocation("");
			setPicture("");
		}
	};
	const fetchData = async () => {
		const locationUrl = "http://localhost:8100/api/locations/";
		const fetchLocationConfig = {
			method: "get",
		};
		const locationResponse = await fetch(locationUrl, fetchLocationConfig);
		if (locationResponse.ok) {
			const data = await locationResponse.json();
			console.log(data);
			setLocations(data.locations);
			console.log(locations);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new hat!</h1>
					<form onSubmit={handleSubmit} id="create-hat-form">
						<div className="form-floating mb-3">
							<input
								value={style}
								onChange={handleStyleChange}
								placeholder="Style"
								required
								type="text"
								name="manufacturer"
								id="style"
								className="form-control"
							/>
							<label htmlFor="stye">Style</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={material}
								onChange={handleMaterialChange}
								placeholder="Material"
								required
								type="text"
								name="name"
								id="material"
								className="form-control"
							/>
							<label htmlFor="material">Material</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={color}
								onChange={handleColorChange}
								placeholder="Color"
								required
								type="text"
								name="color"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={size}
								onChange={handleSizeChange}
								placeholder="Size"
								required
								type="text"
								name="size"
								id="size"
								className="form-control"
							/>
							<label htmlFor="size">Size</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={picture}
								onChange={handlePictureChange}
								placeholder="Image"
								required
								type="url"
								name="picture"
								id="picture"
								className="form-control"
							/>
							<label htmlFor="picture">Picture</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleLocationChange}
								required
								name="location"
								id="location"
								className="form-select"
							>
								<option value={location}>Choose a location</option>
								{locations.map((location) => {
									return (
										<option key={location.id} value={location.href}>
											{location.closet_name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default HatForm;
