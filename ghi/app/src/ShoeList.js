import React, { useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        try {
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                console.log('API Response:', data); // Check the API response
                setShoes(data.Shoes); // Note the capital 'S' in 'Shoes'
            } else {
                console.log('Error:', response.status, response.statusText);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    useEffect(()=> {
        fetchData();
    }, [])
    console.log('shoes:', shoes);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.name }</td>
                            <td>{ shoe.color }</td>
                            <td>{ shoe.picture }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList;
