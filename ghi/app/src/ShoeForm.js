import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
    const [maunfacturer, setManufacturer] = useState('')
    const [name, SetName] = useState('')
    const [color, setColor] = useState('')
    //const [picture, setPicture] = useState('')
    const [bins, setBins] = useState([])
    const [bin, setBin] = useState('')

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(()=> {
        fetchData();
    }, [])

    const handleMaunfacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        SetName(value);
    }
    //const handlePictureChange = (event) => {
    //    const value = event.target.value;
    //    setPicture(value);
    //}
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
            data.maunfacturer = maunfacturer
            data.name = name
            data.color = color
            //data.picture = picture
            data.bin = bin

            const shoeUrl = "http://localhost:8080/api/shoes/";
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch (shoeUrl, fetchConfig);
            if (response.ok) {
                setManufacturer('');
                SetName('');
                setColor('');
                //setPicture('');
                setBin('');
            }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Fill out a shoe form</h1>
              <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                  <input value={maunfacturer} onChange={handleMaunfacturerChange} placeholder="Manufacturer" required type="text" name="Maunfacturer" id="Maunfacturer" className="form-control" />
                  <label htmlFor="name">Manufacturer</label>
                </div>

                <div className="form-floating mb-3">
                  <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="roomCount">Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="city">Color</label>
                </div>

                <div className="mb-3">
                  <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                    <option value="">Select a bin</option>
                    {bins.map(bin => {
                      return (
                        <option key={bin.href} value={bin.href}>
                          {bin.id}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default ShoeForm;
