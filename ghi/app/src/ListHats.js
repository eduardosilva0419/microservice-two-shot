import React, { useState, useEffect } from "react";
import "./index.css";

function ListHats() {
	const [hats, setHats] = useState([]);

	async function fetchHats() {
		const response = await fetch("http://localhost:8090/api/hats/");
		if (response.ok) {
			const parsedJson = await response.json();
			setHats(parsedJson.hats);
		}
	}

	const handleDelete = async (event) => {
		const id = event.target.id;
		const url = `http://localhost:8090/api/hats/${id}/`;
		const fetchConfig = {
			method: "delete",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchHats();
		}
	};

	useEffect(() => {
		fetchHats();
	}, []);
	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Style</th>
					<th>Fabric</th>
					<th>Color</th>
					<th>Size</th>
					<th>Picture</th>
					<th>Location</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				{hats.map((hat) => {
					return (
						<tr key={hat.id}>
							<td>{hat.style}</td>
							<td>{hat.fabric}</td>
							<td>{hat.color}</td>
							<td>{hat.size}</td>
							<td>
								<img src={hat.picture} />
								{}
							</td>
							<td>{hat.location.closet_name}</td>
							<td>
								<button onClick={handleDelete} id={hat.id}>
									Delete
								</button>
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default ListHats;
