import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoeForm from "./ShoeForm";
import ShoeList from "./ShoeList";
import HatForm from "./HatForm";
import ListHats from "./ListHats";

function App(props) {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="/shoes" element={<ShoeList />} />
					<Route path="/shoes/form" element={<ShoeForm />} />
					<Route path="/hats">
						<Route path="" element={<ListHats />} />
						<Route path="new" element={<HatForm />} />
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
